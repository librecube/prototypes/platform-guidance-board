import time
from pyb import LED

import spacecan
import spacecan.services

from functions import perform_function_handler
from parameters import update_parameters


# hardware setup
led_heartbeat = LED(1)


# packet monitor
def packet_monitor(service, subtype, data, node_id):
    print(f"<-({service:02}, {subtype:02}) with [{data}] from {node_id}")


# create node and configure packet utilization protocol
responder = spacecan.Responder.from_file("config/spacecan.json")
pus = spacecan.services.PacketUtilizationServiceResponder(responder)

pus.packet_monitor = packet_monitor
pus.parameter_management.add_parameters_from_file("config/parameters.json")
pus.housekeeping.add_housekeeping_reports_from_file("config/housekeeping.json")


def perform_function(function_id, arguments):
    return perform_function_handler(pus, function_id, arguments)


pus.function_management.add_functions_from_file("config/functions.json")
pus.function_management.perform_function = perform_function

responder.received_heartbeat = lambda: led_heartbeat.toggle()
responder.connect()
responder.start()


# main loop
try:
    print("Running...")
    while True:
        time.sleep(0.1)
        update_parameters(pus)

except KeyboardInterrupt:
    pass

responder.stop()
responder.disconnect()
print("Stopped")
