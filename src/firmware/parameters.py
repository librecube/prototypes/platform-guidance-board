from pyb import LED, Accel
from machine import UART

from alias import *


# hardware setup
accel = Accel()
uart = UART(6, 9600, timeout=100)


def update_parameters(pus):
    set_param = pus.parameter_management.set_parameter_value
    get_param = pus.parameter_management.get_parameter_value

    # update acceleration
    set_param(PARAM_ACC_X, accel.x())
    set_param(PARAM_ACC_Y, accel.y())
    set_param(PARAM_ACC_Z, accel.z())

    # collect gps data
    if get_param(PARAM_GPS_SWITCH_STATUS) == 1:
        lines = []
        while uart.any():  # read lines received from UART
            lines.append(uart.readline())
        for line in lines:
            if line and line.startswith("$GPRMC"):
                line = line.decode()
                _, *data = line.split(",")
                if len(data) > 9:  # ensure that line is complete
                    update_gps(pus, data)
                    print(".", end="")


def update_gps(pus, data):
    set_param = pus.parameter_management.set_parameter_value

    time = float(data[0])
    valid = 1 if data[1] == "A" else 0
    set_param(PARAM_GPS_DATA_VALID, valid)
    set_param(PARAM_GPS_TIME, time)
    if valid:
        latitude = float(data[2]) * (1 if data[3] == "N" else -1)
        longitude = float(data[4]) * (1 if data[5] == "E" else -1)
        set_param(PARAM_GPS_LAT, latitude)
        set_param(PARAM_GPS_LON, longitude)
