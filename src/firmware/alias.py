# map parameter alias to parameter ids
PARAM_GPS_SWITCH_STATUS = 1
PARAM_GPS_DATA_VALID = 2
PARAM_GPS_TIME = 3
PARAM_GPS_LAT = 4
PARAM_GPS_LON = 5
PARAM_ACC_X = 6
PARAM_ACC_Y = 7
PARAM_ACC_Z = 8

# map function alias to function ids
FUNC_GPS_SWITCH_OFF = 1
FUNC_GPS_SWITCH_ON = 2
