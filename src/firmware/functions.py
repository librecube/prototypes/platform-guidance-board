from alias import *


def perform_function_handler(pus, function_id, arguments=None):
    set_param = pus.parameter_management.set_parameter_value
    # get_param = pus.parameter_management.get_parameter_value

    if function_id == FUNC_GPS_SWITCH_OFF:
        set_param(PARAM_GPS_SWITCH_STATUS, 0)
        set_param(PARAM_GPS_DATA_VALID, 0)

    elif function_id == FUNC_GPS_SWITCH_ON:
        set_param(PARAM_GPS_SWITCH_STATUS, 1)

    else:
        # not a valid function id
        return False

    # success
    return True
